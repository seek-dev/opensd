# This project has moved to https://codeberg.org/opensd/opensd


## Why?
1. Thanks to Gitlab's awful CloudFlare reliance, I am not able to log in most of the time.  Sometimes for weeks.

2. Gitlab is demanding that I give them private information for a free account (phone number and credit card).  I refuse.

3. Gitlab now requires all new accounts to provide a verified phone number and valid credit card number.  Never expect anyone to ethically handle information they demand, but don't actually need.

4. I probably wont have the ability to maintain these projects for the next couple years, so I've given them to someone else.

5. Codeberg respects user privacy.

<br>

## Why not keep this as a mirror?

I'm feeling pretty hostile to Gitlab after my last couple conversations with them.  Not as bad as $hithub yet, but they're certainly getting there...
